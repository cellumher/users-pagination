# Listado de Usuarios con Paginación

Proyecto generado con [Angular CLI](https://github.com/angular/angular-cli) versión 16.0.2.

[Click para acceder a la demo](https://users-pagination.netlify.app/)

## Requisitos Previos

Antes de comenzar, asegúrate de tener instalado lo siguiente:

- Node.js (preferiblemente versión LTS)
- npm (se incluye en la instalación de Node.js)
- Angular CLI

Puedes instalar Node.js y npm desde [aquí](https://nodejs.org/), y Angular CLI mediante npm con el siguiente comando:

```bash
npm install -g @angular/cli
```

## Instalación

1. Clona este repositorio

```bash
git clone https://gitlab.com/cellumher/users-pagination
```

2. Entra al directorio raíz del proyecto

```bash
cd users-pagination
```

3. Instala las dependencias del proyecto

```bash
npm i
```

## Servidor de desarrollo

Ejecuta `ng serve` (alternativamente `ng s`) y, cuando esté listo, accede a la url `http://localhost:4200/`.


## Funcionamiento de la aplicación

La aplicación consiste en un listado de usuarios que puede ser filtrado.

![Vista previa de la aplicación](media/application.png)

El filtro, en la parte superior, se aplica al listado de usuarios, que aparecerá en la tabla de la sección inferior.

### Filtros
- **Nombre**: Se aplica al nombre del usuario. Atributo `name`
- **Surname**: Se aplica a la concatenación de los apellidos del usuario separados por un espacio: `surname1 + ' ' + surname2`
- **Email**: Se aplica al email del usuario. Atributo `email`.

Características del filtrado:
- El filtrado no distingue mayúsculas ni minúsculas (case-insensitive). 
- Se hará un trimado de los espacios iniciales y finales de los valores introducidos en los campos. 
- Un campo vacío se interpreta como ausencia de filtro.
- Los filtros se aplican de forma restrictiva: los resultados cumplirán con todos los filtros introducidos (en contraposición a que solo sea necesario uno de ellos).

### Tabla de resultados
La tabla muestra un máximo de cinco resultados por página. En caso de que haya más de una página, se puede avanzar o retroceder con los botones inferiores de Anterior/Siguiente.

## Origen de los datos
Para recoger los datos, se ha simulado una base de datos utilizando la librería `angular-in-memory-web-api`, así como el endpoint POST `api/users`, que recibe los filtros y los aplica antes de devolver los resultados.
