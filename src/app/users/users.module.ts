import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TablePaginationComponent } from '../shared/table-pagination/table-pagination.component';
import { UserTableComponent } from './user-table/user-table.component';
import { UserFilterComponent } from './user-filter/user-filter.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    UserTableComponent,
    UserFilterComponent,
  ],
  imports: [
    CommonModule,
    TablePaginationComponent,
    FormsModule,
  ],
  exports: [
    UserTableComponent,
    UserFilterComponent,
  ]
})
export class UsersModule { }
