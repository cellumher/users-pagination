import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { User } from 'src/app/core/models/user.interface';
import { loadUsers } from 'src/app/core/store/users-table.actions';
import { UsersTableState } from 'src/app/core/store/users-table.reducer';

@Component({
  selector: 'app-user-table',
  templateUrl: './user-table.component.html',
  styleUrls: ['./user-table.component.css']
})
export class UserTableComponent implements OnInit {
  users: User[] = [];
  totalUsers: number = 0;
  loading: boolean = false;
  error: any;
  offset: number = 0;
  pageSize: number = 0;
  totalEmptyRows: number = 0;

  constructor( private store: Store<{ usersTable: UsersTableState}>) { }

  ngOnInit() {

    this.store.select('usersTable').subscribe( ({ users, loading, error, currentPage, pageSize }) => {
      this.offset = (currentPage - 1)*pageSize;
      this.users = users.slice(this.offset, this.offset + pageSize);
      this.loading = loading;
      this.error = error;
      this.pageSize = pageSize;
      this.totalUsers = users.length;

      if (this.users.length && this.users.length < pageSize) {
        const emptyRows: User[] = Array.from({ length: pageSize - this.users.length}, () => ({
          name: "",
          surname1: "",
          surname2: "",
          email: ""
        }))

        this.users = [...this.users, ...emptyRows];
        this.totalEmptyRows = emptyRows.length;
      } else {
        this.totalEmptyRows = 0;
      }
    });

    this.store.dispatch( loadUsers({ userFilter: {
      name: "",
      surnames: "",
      email: "",
    }}) );
  }

}
