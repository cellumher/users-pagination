import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { UserFilter } from 'src/app/core/models/user.interface';
import { UsersTableState } from 'src/app/core/store/users-table.reducer';

@Component({
  selector: 'app-user-filter',
  templateUrl: './user-filter.component.html',
  styleUrls: ['./user-filter.component.css']
})
export class UserFilterComponent {

  userFilter: UserFilter = {
    name: "",
    surnames: "",
    email: "",
  }

  constructor(
    private store: Store<{ usersTable: UsersTableState }>
  ){}

  searchUsers() {

    this.trimLowerCaseFilter();

    this.isFilterAlreadyApplied().subscribe(isApplied => {
      if (!isApplied) {
        this.store.dispatch({
          type: '[Users] Load Users',
          userFilter: { ...this.userFilter }
        });
      }
    });
  }

  private trimLowerCaseFilter() {
    this.userFilter = {
      name: this.userFilter.name?.trim().toLocaleLowerCase(),
      surnames: this.userFilter.surnames?.trim().toLocaleLowerCase(),
      email: this.userFilter.email?.trim().toLocaleLowerCase(),
    }
  }

  private isFilterAlreadyApplied() {
    return this.store.select(state => {
      console.log(state.usersTable.currentFilter)
      console.log(this.userFilter)
      const currentFilter = state.usersTable.currentFilter;
      return currentFilter.name === this.userFilter.name &&
        currentFilter.email === this.userFilter.email &&
        currentFilter.surnames === this.userFilter.surnames;
    });
  }
}
