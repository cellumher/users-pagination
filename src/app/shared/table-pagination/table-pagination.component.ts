import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { UsersTableState } from 'src/app/core/store/users-table.reducer';
import { nextPage, previousPage } from '../../core/store/users-table.actions';
import { FormsModule } from '@angular/forms';
import { NgClass } from '@angular/common';

@Component({
  selector: 'shared-table-pagination',
  templateUrl: './table-pagination.component.html',
  styleUrls: ['./table-pagination.component.css'],
  standalone: true,
  imports: [FormsModule, NgClass]
})
export class TablePaginationComponent {

  error: any;
  currentPage: number = 0;
  totalPages: number = 0;

  constructor( private store: Store<{ usersTable: UsersTableState}>) { }

  ngOnInit() {

    this.store.select('usersTable').subscribe( ({ error, currentPage, totalPages }) => {
      this.currentPage = currentPage;
      this.totalPages = totalPages;
      this.error = error;
    });

  }

  nextPage() {
    this.store.dispatch( nextPage() );
  }

  previousPage() {
    this.store.dispatch( previousPage() );
  }


}
