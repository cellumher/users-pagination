export interface User {
  id?: number;
  email: string;
  name: string;
  surname1: string;
  surname2: string;
}

export interface UserFilter {
  email?: string;
  name?: string;
  surnames?: string;
}
