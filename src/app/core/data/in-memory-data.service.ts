import { InMemoryDbService, RequestInfo } from 'angular-in-memory-web-api';
import { User, UserFilter } from '../models/user.interface';
import { Observable, of } from 'rxjs';
import { HttpRequest } from '@angular/common/http';

export class InMemoryDataService implements InMemoryDbService {

  private users: User[] = [
    {
      id: 1,
      email: "admin1@yopmail.com",
      name: "admin1",
      surname1: "admin1",
      surname2: "admin1"
    },
    {
      id: 2,
      email: "admin2@yopmail.com",
      name: "admin2",
      surname1: "admin2",
      surname2: "admin2"
    },
    {
      id: 3,
      email: "admin3@yopmail.com",
      name: "admin3",
      surname1: "admin3",
      surname2: "admin3"
    },
    {
      id: 4,
      email: "admin4@yopmail.com",
      name: "admin4",
      surname1: "admin4",
      surname2: "admin4"
    },
    {
      id: 5,
      email: "admin5@yopmail.com",
      name: "admin5",
      surname1: "admin5",
      surname2: "admin5"
    },
    {
      id: 6,
      email: "admin6@yopmail.com",
      name: "admin6",
      surname1: "admin6",
      surname2: "admin6"
    },
    {
      id: 7,
      email: "admin7@yopmail.com",
      name: "admin7",
      surname1: "admin7",
      surname2: "admin7"
    },
    {
      id: 8,
      email: "admin8@yopmail.com",
      name: "admin8",
      surname1: "admin8",
      surname2: "admin8"
    },
    {
      id: 9,
      email: "admin9@yopmail.com",
      name: "admin9",
      surname1: "admin9",
      surname2: "admin9"
    },
    {
      id: 10,
      email: "user1@yopmail.com",
      name: "user1",
      surname1: "user1",
      surname2: "user1"
    },
    {
      id: 11,
      email: "user2@yopmail.com",
      name: "user2",
      surname1: "user2",
      surname2: "user2"
    },
    {
      id: 12,
      email: "user3@yopmail.com",
      name: "user3",
      surname1: "user3",
      surname2: "user3"
    },
    {
      id: 13,
      email: "user4@yopmail.com",
      name: "user4",
      surname1: "user4",
      surname2: "user4"
    },
    {
      id: 14,
      email: "user5@yopmail.com",
      name: "user5",
      surname1: "user5",
      surname2: "user5"
    },
    {
      id: 15,
      email: "user6@yopmail.com",
      name: "user6",
      surname1: "user6",
      surname2: "user6"
    },
    {
      id: 16,
      email: "user7@yopmail.com",
      name: "user7",
      surname1: "user7",
      surname2: "user7"
    },
    {
      id: 17,
      email: "user8@yopmail.com",
      name: "user8",
      surname1: "user8",
      surname2: "user8"
    },
    {
      id: 18,
      email: "user9@yopmail.com",
      name: "user9",
      surname1: "user9",
      surname2: "user9"
    },

  ];

  createDb() {
    return { users: this.users };
  };

  post(reqInfo: RequestInfo): Observable<User[]> {
    if (reqInfo.collectionName === 'users') {
      const userFilter = (reqInfo.req as HttpRequest<any>).body
      const users = this.getUsersFilteredFromDatabase(userFilter);

      return reqInfo.utils.createResponse$(() => ({
        status: 200,
        body: users
      }))
    }
    return of([]);
  }

  private getUsersFilteredFromDatabase(userFilter: UserFilter): User[] {
    const result = this.users.filter(user => {
      const userSurnames = user.surname1.concat(' ', user.surname2);

      return (
        user.name.toLocaleLowerCase().includes(
          userFilter.name?.toLocaleLowerCase().trim() ?? "")
        && userSurnames.toLocaleLowerCase().includes(
          userFilter.surnames?.toLocaleLowerCase().trim() ?? "")
        && user.email.toLocaleLowerCase().includes(
          userFilter.email?.toLocaleLowerCase().trim() ?? "")
      );
    });

    return result;
  }

}
