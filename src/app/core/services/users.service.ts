import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User, UserFilter } from '../models/user.interface';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor(private http: HttpClient) { };

  getUsersFiltered(userFilter: UserFilter) {
    return this.http.post<User[]>('api/users', userFilter);
  }

}
