import { createAction, props } from '@ngrx/store';
import { User, UserFilter } from '../models/user.interface';

export const loadUsers = createAction(
  '[Users] Load Users',
  props<{ userFilter: UserFilter }>()
  );

export const loadUsersSuccess = createAction(
    '[Users] Load Users Success',
    props<{ users: User[] }>()
);

export const loadUsersError = createAction(
    '[Users] Load Users Error',
    props<{ payload: any }>()
);

export const nextPage = createAction(
  '[Users] Navigate to next page'
);

export const previousPage = createAction(
  '[Users] Navigate to previous page'
);

