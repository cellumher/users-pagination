import { createReducer, on } from '@ngrx/store';
import { User, UserFilter } from '../models/user.interface';
import * as userActions from './users-table.actions';

export interface UsersTableState {
    users  : User[];
    loading: boolean;
    error  : any;
    totalPages: number;
    currentPage: number;
    pageSize: number;
    currentFilter: UserFilter;
}

export const usersTableInitialState: UsersTableState = {
    users: [],
    loading: false,
    error: null,
    totalPages: 0,
    currentPage: 0,
    pageSize: 5,
    currentFilter: {},
}

export const usersTableReducer = createReducer(
  usersTableInitialState,

  on( userActions.loadUsers, (state, { userFilter }) => ({
    ...state,
    loading: true,
    currentFilter: {...userFilter},
  })),

  on( userActions.loadUsersSuccess, (state, { users }) => ({
    ...state,
    loading: false,
    users: [ ...users ],
    totalPages: Math.ceil(users.length / state.pageSize),
    currentPage: 1,
    error: null,
  })),

  on( userActions.loadUsersError, (state, { payload }) => ({
    ...state,
    loading: false,
    error: {
        ...payload,
    }
  })),

  on( userActions.nextPage, (state) => ({
    ...state,
    currentPage: Math.min(state.currentPage + 1, state.totalPages),
  })),

  on( userActions.previousPage, (state) => ({
    ...state,
    currentPage: Math.max(state.currentPage - 1, 1),
  })),
);
