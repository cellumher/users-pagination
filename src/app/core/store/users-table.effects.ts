import { Injectable } from '@angular/core';
import { mergeMap, map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import * as userActions from './users-table.actions';
import { User } from '../models/user.interface';
import { TypedAction } from '@ngrx/store/src/models';
import { UsersService } from '../services/users.service';
import { Actions, createEffect, ofType } from '@ngrx/effects';



@Injectable()
export class UsersEffects {

  constructor(
    private actions$: Actions,
    private usersService: UsersService
  ){}


  loadUsers$ = createEffect(
    () => this.actions$.pipe(
      ofType( userActions.loadUsers ),
      mergeMap(
        (action) => this.usersService.getUsersFiltered(action.userFilter)
          .pipe(
            map<User[], TypedAction<'[Users] Load Users Success'>>( users => userActions.loadUsersSuccess({ users }) ),
            catchError( err => of(userActions.loadUsersError({ payload: err })) )
          )
      )
    )
  );

}
